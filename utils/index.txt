#https://qbox.io/blog/an-introduction-to-ngrams-in-elasticsearch


#DELETE /profile
PUT /profile
{
  "settings": {
    "number_of_shards": 1,
    "max_ngram_diff": 10,
    "analysis": {
      "tokenizer": {
        "ngram_tokenizer": {
          "type": "nGram",
          "min_gram": 2,
          "max_gram": 10,
          "token_chars": [
            "letter",
            "digit"
          ]
        }
      },
      "analyzer": {
        "ngram_tokenizer_analyzer": {
          "type": "custom",
          "tokenizer": "ngram_tokenizer",
          "filter": [
            "lowercase"
          ]
        }
      }
    }
  },
  "mappings": {
    "properties": {
      "name": {
        "type": "text",
        "term_vector": "yes",
        "analyzer": "ngram_tokenizer_analyzer"
      },
      "username": {
        "type": "text",
        "term_vector": "yes",
        "analyzer": "ngram_tokenizer_analyzer"
      }
    }
  }
}
