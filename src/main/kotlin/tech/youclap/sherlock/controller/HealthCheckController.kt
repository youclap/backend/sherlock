package tech.youclap.sherlock.controller

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus.NO_CONTENT
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("healthcheck")
class HealthCheckController {

    private companion object {
        private val logger = LoggerFactory.getLogger(HealthCheckController::class.java)
    }

    @GetMapping
    @ResponseStatus(NO_CONTENT)
    fun healthcheck() {
        logger.info("🚑 Health check")
    }
}
