package tech.youclap.sherlock

import org.apache.http.HttpHost
import org.elasticsearch.client.RestClient
import org.elasticsearch.client.RestHighLevelClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class Provider {

    @Bean(destroyMethod = "close")
    fun restHighLevelClient(
        @Value("\${elasticsearch.hostname}") host: String,
        @Value("\${elasticsearch.port}") port: Int
    ): RestHighLevelClient {

        val restClientBuilder = RestClient
            .builder(HttpHost(host, port))

//        TODO Do something with this values?
//            .setRequestConfigCallback { config ->
//                config
//                    .setConnectTimeout(5000)
//                    .setConnectionRequestTimeout(5000)
//                    .setSocketTimeout(5000)
//            }
//            .setMaxRetryTimeoutMillis(5000)

        return RestHighLevelClient(restClientBuilder)
    }
}
