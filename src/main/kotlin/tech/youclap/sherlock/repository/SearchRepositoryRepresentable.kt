package tech.youclap.sherlock.repository

import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import java.math.BigInteger

@Repository
interface SearchRepositoryRepresentable {
    fun findByTerm(term: String): Mono<List<BigInteger>>
}
