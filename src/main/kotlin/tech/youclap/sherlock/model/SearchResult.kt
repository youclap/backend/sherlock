package tech.youclap.sherlock.model

import java.math.BigInteger

data class SearchResult(
    val profileIDs: List<BigInteger>
)
