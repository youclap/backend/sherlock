package tech.youclap.sherlock.service

import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import tech.youclap.sherlock.model.SearchResult
import tech.youclap.sherlock.repository.SearchRepositoryRepresentable

@Service
class SearchService(
    private val searchRepository: SearchRepositoryRepresentable
) : SearchServiceRepresentable {

    override fun search(term: String): Mono<SearchResult> {

        return searchRepository
            .findByTerm(term)
            .map {
                SearchResult(
                    profileIDs = it
                )
            }
    }
}
