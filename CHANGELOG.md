# CHANGELOG

<!--- next entry here -->

## 0.1.0
2019-09-18

### Features

- **backend-78:** Search users (4f793328373a6f030b3a0c51b0e742f491363cf8)
- **backend-222:** Inject service configs through Helm (65a0d431d9999b3540d04692dbaac38837617637)
- Updated helm chart and renamed profiles (19f7e2cfa632f144f8a5cf600ad37031ed77ff55)